//Экранирование нужно для того, чтобы строка, в которой есть спецсимволы, отображалась как текст. 

function createNewUser() { // функция создает и
	const firstName = prompt('What is your name?'), // функция спрашивает у вызывающего имя
		lastName = prompt('What is your last name?'), // и фамилию.
		birthday = prompt('What is your date of birth?', 'dd.mm.yyyy').trim(),
		date = new Date(),
		userDayOfBirth = birthday.slice(0, 2),
		userMonthOfBirth = birthday.slice(3, 5),
		userYearOfBirth = birthday.slice(6),
		currentMonthOfYear = date.getMonth() + 1;

	return newUser = { // возвращает объект `newUser` со свойствами
		firstName, // `firstName` и
		lastName, // `lastName`
		birthday, // `birthday`
		getPassword() { // добавить в объект`newUser` метод`getLogin()
			const result = firstName.slice(0, 1).toUpperCase() + lastName.toLowerCase() + userYearOfBirth;
			return console.log(result); // вернуть первую букву имени пользователяв верхнем регистре,
			// соединенную с фамилией (в нижнем регистре)
		},
		getAge() {
			let userAge = date.getFullYear() - userYearOfBirth; // Определяем userAge на основе года

			if (userMonthOfBirth > (currentMonthOfYear)) {// Определяем userAge на основе месяца
				userAge--;
			}
			// console.log(typeof userMonthOfBirth);
			// console.log(typeof currentMonthOfYear);
			// userMonthOfBirth, currentMonthOfYear разные типы данных (==)

			// Определяем userAge на основе дня
			if ((userMonthOfBirth == currentMonthOfYear) && (userDayOfBirth > date.getDate())) {
				userAge--;
			}
			return console.log(userAge + ' лет');
			//return console.log(userAge, date.getDate(), currentMonthOfYear, date.getFullYear());
		},
	}
}
const user = createNewUser();
//console.log(user); // вывод объекта `newUser` пользователя
user.getPassword();
user.getAge();



